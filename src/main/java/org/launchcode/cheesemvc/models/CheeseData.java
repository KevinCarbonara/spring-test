package org.launchcode.cheesemvc.models;

import java.util.ArrayList;

public class CheeseData {
    static ArrayList<Cheese> cheeses = new ArrayList();

    public static ArrayList<Cheese> getAll() {
        return cheeses;
    }

    public static void add(Cheese cheese) {
        cheeses.add(cheese);
    }

    public static void remove(int id) {
        cheeses.remove(getById(id));
    }

    public static Cheese getById(int id) {
        for(Cheese cheese : cheeses) {
            if (cheese.getCheeseId() == id)
                return cheese;
        }

        return null;
    }
}
